import * as React from 'react';
import Box from '@mui/material/Box';
import BottomNavigation from '@mui/material/BottomNavigation';
import BottomNavigationAction from '@mui/material/BottomNavigationAction';
// import RestoreIcon from '@mui/icons-material/Restore';
// import FavoriteIcon from '@mui/icons-material/Favorite';
// import LocationOnIcon from '@mui/icons-material/LocationOn';
import {useNavigate} from "react-router-dom";


const BottomNavBar = () => {
  const [value, setValue] = React.useState(0);
  const navigate = useNavigate();

  return (
    <Box>
      <BottomNavigation
        showLabels
        value={value}
        onChange={(event, newValue) => {
          setValue(newValue);
        }}
      >

        <BottomNavigationAction label="Overview"  onClick={() => 
          navigate(`/`)
          } />
        <BottomNavigationAction label="Matzal"  onClick={() => 
          navigate(`/matzal`)
          } />
        <BottomNavigationAction label="Tests" onClick={() => 
          navigate(`/tests`)
          } />
      </BottomNavigation>
    </Box>
  );
}

export default BottomNavBar;