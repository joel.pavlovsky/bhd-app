import React from 'react';
import './App.css';
import { Routes, Route} from "react-router-dom";
import Overview from './Components/Overview/Overview';
import Matzal from './Components/Matzal/Matzal';
import Tests from './Components/Tests/Tests';
import BottomNavBar from './Components/BottomNavBar/BottomNavBar';

function App() {
  return (
    <div className="App">
      <div>
        
      <Routes>
        <Route path="/" element={<Overview />}/>
        <Route path="/matzal" element={<Matzal />} />
        <Route path="/tests" element={<Tests />} />
      </Routes>
      <BottomNavBar/>
      </div>
    </div>
  );
}

export default App;
